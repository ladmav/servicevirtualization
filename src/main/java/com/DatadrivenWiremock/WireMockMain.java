package com.DatadrivenWiremock;

import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;

import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

public class WireMockMain {

    private static final WireMockServer wireMockServer = new WireMockServer(options().port(9081));


    public static void main(String[] args) throws FilloException {

        wireMockServer.start();
        WireMock.configureFor("localhost", 9081);
        MethodInstantiation M1 = new MethodInstantiation();

        List<String> listMethods = new ArrayList<>();
        List<String> listurl = new ArrayList<>();
        List<String> listRequestBody = new ArrayList<>();
        List<String> listResponseBody = new ArrayList<>();
        List<String> ResponseCodes = new ArrayList<>();
        List<String> ContentType = new ArrayList<>();
        List<String> QueryParamk = new ArrayList<>();
        List<String> QueryParamv = new ArrayList<>();

        Fillo fillo = new Fillo();
        Connection connection = fillo.getConnection("C:\\Users\\syeda\\Desktop\\Techriot_Test_data_Template.xlsm");
        String strQuery = "Select * from Configuration";
        Recordset recordset = connection.executeQuery(strQuery);

        while (recordset.next()) {
            listurl.add(recordset.getField("URI"));
            listRequestBody.add(recordset.getField("Request Body"));
            listResponseBody.add(recordset.getField("Response Body"));
            listMethods.add(recordset.getField("HTTP Method"));
            ResponseCodes.add(recordset.getField("Response Codes"));
            ContentType.add(recordset.getField("Content Type"));
            QueryParamk.add(recordset.getField("Query Parameter Key"));
            QueryParamv.add(recordset.getField("Query Parameter Value"));
        }

        for(int i=0;i<listMethods.size();i++)
        {
            if(QueryParamk.get(i).isEmpty()) {
                M1.SwitchMethods(listurl.get(i), listMethods.get(i), listRequestBody.get(i), listResponseBody.get(i), ResponseCodes.get(i), ContentType.get(i));
            }
            else{
                M1.SwitchMethods(listurl.get(i), listMethods.get(i), listRequestBody.get(i), listResponseBody.get(i), ResponseCodes.get(i), ContentType.get(i),QueryParamk.get(i),QueryParamv.get(i));
            }
        }
    }



}
