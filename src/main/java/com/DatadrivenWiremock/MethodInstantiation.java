package com.DatadrivenWiremock;

public class MethodInstantiation {

    Methods M = new Methods();

    public void SwitchMethods(String URI, String method, String RequestBody, String ResponseBody, String ResponseCode, String Contenttype)
    {
        switch (method) {
            case "POST":
                M.PostMethod(URI,RequestBody,ResponseBody,ResponseCode,Contenttype);
                break;
            case "GET":
                M.GetMethod(URI,ResponseBody,ResponseCode,Contenttype);
                break;
            case "PUT":
                M.PutMethod(URI,RequestBody,ResponseBody,ResponseCode,Contenttype);
                break;
            case "DELETE":
                M.DeleteMethod(URI,RequestBody,ResponseBody,ResponseCode,Contenttype);
                break;
            default:
                System.out.println("Unavailable method");
        }

    }

    public void SwitchMethods(String URI, String method, String RequestBody, String ResponseBody, String ResponseCode, String Contenttype, String key,String Value) {
        switch (method) {
            case "POST":
                M.PostMethod(URI, RequestBody, ResponseBody, ResponseCode, Contenttype, key, Value);
                break;
            case "GET":
                M.GetMethod(URI, ResponseBody, ResponseCode, Contenttype, key, Value);
                break;
            case "PUT":
                M.PutMethod(URI, RequestBody, ResponseBody, ResponseCode, Contenttype, key, Value);
                break;
            case "DELETE":
                M.DeleteMethod(URI, RequestBody, ResponseBody, ResponseCode, Contenttype, key, Value);
                break;
            default:
                System.out.println("Unavailable method");


        }
    }
}
