package com.DatadrivenWiremock;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class Methods {

    public void PostMethod(String URI, String RequestBody, String ResponseBody, String ResponseCode, String Contenttype){

        stubFor(post(urlEqualTo(URI))
                .withHeader("Content-Type", equalTo(Contenttype))
                .withRequestBody(containing(RequestBody))
                .willReturn(aResponse().withBody(ResponseBody).withStatus(Integer.parseInt(ResponseCode))));
    }

    public void GetMethod(String URI, String ResponseBody, String ResponseCode, String Contenttype)
    {
        stubFor(get(urlEqualTo(URI)).willReturn(aResponse().withStatus(Integer.parseInt(ResponseCode))
                .withHeader("Content-Type", Contenttype).withBody(ResponseBody)));
    }
    public void PutMethod(String URI, String RequestBody, String ResponseBody, String ResponseCode, String Contenttype)
    {
        stubFor(put(urlEqualTo(URI))
                .withRequestBody(containing(RequestBody)).willReturn(aResponse().withStatus(Integer.parseInt(ResponseCode)).withBody(ResponseBody)
                        .withHeader("Content-Type",Contenttype)));
    }

    public void DeleteMethod(String URI, String RequestBody, String ResponseBody, String ResponseCode, String Contenttype)
    {
        stubFor(delete(urlEqualTo(URI))
                .withRequestBody(containing(RequestBody)).willReturn(aResponse().withStatus(Integer.parseInt(ResponseCode)).withBody(ResponseBody)
                        .withHeader("Content-Type",Contenttype)));
    }

    public void PostMethod(String URI, String RequestBody, String ResponseBody, String ResponseCode, String Contenttype,String key,String Value){
        stubFor(post(urlPathMatching(URI)).withQueryParam(key,equalTo(Value))
                .withHeader("Content-Type", equalTo(Contenttype))
                .withRequestBody(containing(RequestBody))
                .willReturn(aResponse().withBody(ResponseBody).withStatus(Integer.parseInt(ResponseCode))));
    }
    public void GetMethod(String URI, String ResponseBody, String ResponseCode, String Contenttype,String key,String Value)
    {
        stubFor(get(urlPathMatching(URI)).withQueryParam(key,equalTo(Value)).willReturn(aResponse().withStatus(Integer.parseInt(ResponseCode))
                .withHeader("Content-Type", Contenttype).withBody(ResponseBody)));
    }
    public void PutMethod(String URI, String RequestBody, String ResponseBody, String ResponseCode, String Contenttype,String key,String Value)
    {
        stubFor(put(urlEqualTo(URI)).withQueryParam(key,equalTo(Value))
                .withRequestBody(containing(RequestBody)).willReturn(aResponse().withStatus(Integer.parseInt(ResponseCode))
                        .withHeader("Content-Type",Contenttype)));
    }

    public void DeleteMethod(String URI, String RequestBody, String ResponseBody, String ResponseCode, String Contenttype,String key,String Value)
    {
        stubFor(delete(urlPathMatching(URI)).withQueryParam(key,equalTo(Value)).withRequestBody(containing(RequestBody))
                .willReturn(aResponse().withStatus(Integer.parseInt(ResponseCode)).withHeader("Content-Type",Contenttype)
                        .withBody(ResponseBody)));
    }

}
